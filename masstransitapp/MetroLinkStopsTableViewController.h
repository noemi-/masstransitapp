//
//  MetroLinkStopsTableViewController.h
//  masstransitapp
//
//  Created by Noemi Quezada on 12/1/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Route.h"
#import "Stop.h"
#import "MetroDatabase.h"
#import "UIColor+HexadecimalColor.h"
#import "StopsStopCodeBadge.h"

@interface MetroLinkStopsTableViewController : UITableViewController

@property(nonatomic)NSArray * stops;
@property (nonatomic, strong)Route * aRoute;


@end
