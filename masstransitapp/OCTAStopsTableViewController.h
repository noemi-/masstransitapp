//
//  OCTAStopsTableViewController.h
//  masstransitapp
//
//  Created by Noemi Quezada on 11/23/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Route.h"


@interface OCTAStopsTableViewController : UITableViewController

@property(nonatomic)NSArray * stops;
@property (nonatomic, strong)Route * aRoute;



@end
