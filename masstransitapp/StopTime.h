//
//  StopTime.h
//  masstransitapp
//
//  Created by Noemi Quezada on 11/22/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Trip.h"

@interface StopTime : NSObject

@property(strong, nonatomic)Trip * trip;
@property(strong, nonatomic)NSString * arrivalTime;
@property(strong, nonatomic)NSString * departureTime;


-(StopTime *)initWithArrivalTime:(NSString *)anArrivalTime withDepartureTime:(NSString *)aDeparturedTime andWithATrip:(Trip *)aTrip;
@end
