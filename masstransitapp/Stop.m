//
//  Stop.m
//  masstransitapp
//
//  Created by Noemi Quezada on 11/22/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "Stop.h"

@implementation Stop

@synthesize routeId;
@synthesize tripIds;
@synthesize stopId;
@synthesize stopName;
@synthesize stopCode; 
@synthesize stopSequence;



-(Stop *)initWithRouteId:(NSString *)aRouteId withArrayOfTripIds:(NSArray *)TripIdsArray withStopId:(NSString *)aStopId withStopCode:(NSString *)aStopCode withStopName:(NSString *)aStopName withStopSequence:(NSInteger)aStopSequence
{
    self = [super init];
    if (self)
    {
        routeId = aRouteId;
        tripIds = [[NSArray alloc]initWithArray:TripIdsArray];
        stopId = aStopId;
        stopCode = aStopCode;
        stopName = aStopName;
        stopSequence = aStopSequence;


    }
    return self; 
}

@end
