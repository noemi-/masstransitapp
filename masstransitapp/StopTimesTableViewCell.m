//
//  StopTimesTableViewCell.m
//  masstransitapp
//
//  Created by Noemi Quezada on 11/27/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "StopTimesTableViewCell.h"

@implementation StopTimesTableViewCell

@synthesize MondayLabel;
@synthesize TuesdayLabel;
@synthesize ThursdayLabel;
@synthesize WednesdayLabel;
@synthesize FridayLabel;
@synthesize SaturdayLabel;
@synthesize SundayLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
