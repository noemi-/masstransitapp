//
//  Trip.h
//  masstransitapp
//
//  Created by Noemi Quezada on 11/22/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Trip : NSObject

@property(strong, nonatomic)NSString * routeId;
@property(assign, nonatomic)NSInteger  tripId;
@property(strong, nonatomic)NSArray * calendarDays;

-(Trip *)initWithRouteId: (NSString *)aRouteId withCalendarDays:(NSArray *)someCalendarDays andWithTripId:(NSInteger)aTripId;

@end
