//
//  PDFViewController.h
//  masstransitapp
//
//  Created by Noemi Quezada on 11/28/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDFScrollView.h"

@interface OCTAPDFViewController : UIViewController <UITabBarDelegate>

@end
