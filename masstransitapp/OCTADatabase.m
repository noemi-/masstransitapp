//
//  OCTADatabase.m
//  masstransitapp
//
//  Created by Noemi Quezada on 11/17/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "OCTADatabase.h"

static OCTADatabase* _databaseObj;

@implementation OCTADatabase



+ (OCTADatabase*) database
{
    if (_databaseObj == nil) {
        _databaseObj = [[OCTADatabase alloc] initWithFile:@"OCTA"];
    }
    return _databaseObj;
}

-(id)init
{
    self = [super init];
    if (self)
    {
        self.fileName = @"OCTA";
    }
    
    return self; 
}

@end
