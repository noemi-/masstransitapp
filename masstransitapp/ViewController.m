//
//  ViewController.m
//  masstransitapp
//
//  Created by Noemi Quezada on 11/13/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "ViewController.h"
#import "OCTADatabase.h"
#import "Route.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    OCTADatabase * aDB = [[OCTADatabase alloc ]init];
    //[aDB initWithPath:@"OCTA"];
    NSArray * anArray = [[NSArray alloc]initWithArray:[aDB allRoutes]];
    
    NSLog(@"Hello I am in here");
    for (id object in anArray)
    {
        Route * aRoute = object;
        NSLog(@"Route Id: %@ with Route Short Name: %@ and Route Color: %@", aRoute.routeId, aRoute.routeShortName, aRoute.routeColor);
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
