//
//  RouteShortNameBadge.m
//  masstransitapp
//
//  Created by Noemi Quezada on 11/19/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "RouteShortNameBadge.h"
#import "UIColor+HexadecimalColor.h"

@implementation RouteShortNameBadge

@synthesize hexColor;
@synthesize text;
@synthesize textColor; 

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.clearsContextBeforeDrawing = YES;
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code

    
    /* Font for Label */
    UIFont *textFont = [UIFont systemFontOfSize:19];
    
    /* Color for Circle */
    UIColor *circleColor = [UIColor colorWithHexadecimalString:hexColor];
    
    CGColorRef color = [circleColor CGColor];
    CGFloat circleRed;
    CGFloat circleGreen;
    CGFloat circleBlue;
    CGFloat circleAlpha;
    
    size_t colorComponentCount = CGColorGetNumberOfComponents(color);
    
    if (colorComponentCount == 4)
    {
        const CGFloat * colorComponents = CGColorGetComponents(color);
        circleRed = colorComponents[0];
        circleGreen = colorComponents[1];
        circleBlue = colorComponents[2];
        circleAlpha = colorComponents[3];
    }
    
    

    
    /* Color for Text */
    UIColor * aTextColor = [UIColor colorWithHexadecimalString:textColor];
    
    /* Get a Context First*/
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetRGBFillColor(context, circleRed, circleGreen, circleBlue,  1.0);
    
    CGContextFillEllipseInRect(context, CGRectMake(0, 0, 45, 45));
    
    CGFloat fontHeight = textFont.pointSize;
    /* Calculate the offset for the text */
    CGFloat yOffset = ((rect.size.height-4) - fontHeight) / 2.0;
    
    /* Create a rectangle */
    CGRect textRect = CGRectMake(0, yOffset, rect.size.width+1, fontHeight);
    
    /* Style for String */
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setAlignment:NSTextAlignmentCenter];
    
    /* Draw the text in the rectangle */
    [text drawInRect:textRect withAttributes:@{NSFontAttributeName:textFont, NSForegroundColorAttributeName:aTextColor, NSParagraphStyleAttributeName:style}];
    

}


@end
