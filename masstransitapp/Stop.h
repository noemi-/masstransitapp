//
//  Stop.h
//  masstransitapp
//
//  Created by Noemi Quezada on 11/22/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StopTime.h"

@interface Stop : NSObject

@property(strong, nonatomic)NSString * routeId;
@property(strong, nonatomic)NSArray * tripIds;
@property(strong, nonatomic)NSString * stopId;
@property(strong, nonatomic)NSString * stopCode;
@property(strong, nonatomic)NSString * stopName;
@property(assign, nonatomic)NSInteger stopSequence;



-(Stop *)initWithRouteId:(NSString *)aRouteId withArrayOfTripIds: (NSArray *) TripIdsArray withStopId: (NSString *)aStopId withStopCode: (NSString *)aStopCode withStopName:(NSString *)aStopName withStopSequence:(NSInteger)aStopSequence;

@end
