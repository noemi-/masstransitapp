//
//  TextView.m
//  masstransitapp
//
//  Created by Noemi Quezada on 12/1/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "TextView.h"
#import "UIColor+HexadecimalColor.h"

@implementation TextView

@synthesize text;
@synthesize textColor; 

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
    /* Font for Label */
    UIFont *textFont = [UIFont systemFontOfSize:14];
    
    /* Color for Text */
    UIColor * aTextColor = [UIColor colorWithHexadecimalString:textColor];
        
    CGFloat fontHeight = textFont.pointSize;
    /* Calculate the offset for the text */
    CGFloat yOffset = ((rect.size.height-4) - fontHeight) / 2.0;
    
    /* Create a rectangle */
    CGRect textRect = CGRectMake(0, yOffset, rect.size.width+1, fontHeight);
    
    /* Style for String */
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setAlignment:NSTextAlignmentCenter];
    
    /* Draw the text in the rectangle */
    [text drawInRect:textRect withAttributes:@{NSFontAttributeName:textFont, NSForegroundColorAttributeName:aTextColor, NSParagraphStyleAttributeName:style}];
}


@end
