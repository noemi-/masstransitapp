//
//  RouteShortNameBadge.h
//  masstransitapp
//
//  Created by Noemi Quezada on 11/19/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RouteShortNameBadge : UIView

@property(strong, nonatomic)NSString * hexColor;
@property(strong, nonatomic)NSString * textColor; 
@property(strong, nonatomic)NSString * text;

@end
