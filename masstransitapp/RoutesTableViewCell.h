//
//  RoutesTableViewCell.h
//  masstransitapp
//
//  Created by Noemi Quezada on 11/19/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RouteShortNameBadge.h"

@interface RoutesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *routeLongNameLabel; 
@property (weak, nonatomic) IBOutlet RouteShortNameBadge *routeShortNameBadgeImage;
@property (weak, nonatomic) IBOutlet UILabel *routeDescriptionLabel;



@end
