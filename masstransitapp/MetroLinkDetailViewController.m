//
//  MetroLinkDetailViewController.m
//  masstransitapp
//
//  Created by Noemi Quezada on 12/1/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "MetroLinkDetailViewController.h"

@interface MetroLinkDetailViewController ()

@end

@implementation MetroLinkDetailViewController

@synthesize aRoute;
@synthesize aStop;
@synthesize times;
@synthesize routeNumberLabel;
@synthesize StopNameLabel;
@synthesize StopTimesTable;
@synthesize routeDescriptionLabel;
@synthesize stopCodeLabel;
@synthesize RouteNameLabel;
@synthesize StopTimesUIView;
@synthesize stopLabel;
@synthesize stopTimesLabel;
@synthesize StopUIView;
@synthesize routeLabel;
@synthesize RouteUIView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.


        MetroDatabase * aDatabase = [MetroDatabase database];
        times = [[NSArray alloc]initWithArray:[aDatabase stopTimesForStop:aStop]];
        RouteNameLabel.text = @"";
        routeDescriptionLabel.text = aRoute.routeLongName;
        
    
    
    StopNameLabel.text = aStop.stopName;
    routeNumberLabel.text = aRoute.routeShortName;
    stopCodeLabel.text = aStop.stopCode;
    
    routeNumberLabel.textColor = [UIColor colorWithHexadecimalString:aRoute.routeTextColor];
    stopCodeLabel.textColor = [UIColor colorWithHexadecimalString:aRoute.routeTextColor];
    
    stopLabel.textColor = [UIColor colorWithHexadecimalString:aRoute.routeTextColor];
    routeLabel.textColor =[UIColor colorWithHexadecimalString:aRoute.routeTextColor];
    stopTimesLabel.textColor = [UIColor colorWithHexadecimalString:aRoute.routeTextColor];
    
    RouteUIView.backgroundColor = [UIColor colorWithHexadecimalString:aRoute.routeColor];
    StopTimesUIView.backgroundColor =[UIColor colorWithHexadecimalString:aRoute.routeColor];
    StopUIView.backgroundColor = [UIColor colorWithHexadecimalString:aRoute.routeColor];
    
    
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return [times count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    StopTimesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TimesCell" forIndexPath:indexPath];
    
    
    StopTime *time = (times)[indexPath.row];
    Trip * trip = [time trip];
    
    cell.ArrivalTimeLabel.text = time.arrivalTime;
    cell.DepartureTimeLabel.text = time.departureTime;
    cell.calendarView.backgroundColor = [UIColor colorWithHexadecimalString:aRoute.routeColor];
    
    if ([[trip.calendarDays objectAtIndex:0]integerValue] == 1)
    {
        cell.MondayLabel.textColor = [UIColor colorWithHexadecimalString:aRoute.routeTextColor];
        
        
    }
    else{
        cell.MondayLabel.textColor = [UIColor colorWithHexadecimalString:aRoute.routeColor];
    }
    if ([[trip.calendarDays objectAtIndex:1]integerValue] == 1)
    {
        cell.TuesdayLabel.textColor = [UIColor colorWithHexadecimalString:aRoute.routeTextColor];
    }
    else
    {
        cell.TuesdayLabel.textColor = [UIColor colorWithHexadecimalString:aRoute.routeColor];
    }
    if ([[trip.calendarDays objectAtIndex:2]integerValue] == 1)
    {
        cell.WednesdayLabel.textColor = [UIColor colorWithHexadecimalString:aRoute.routeTextColor];
    }
    else{
        cell.WednesdayLabel.textColor = [UIColor colorWithHexadecimalString:aRoute.routeColor];
    }
    if ([[trip.calendarDays objectAtIndex:3]integerValue] == 1)
    {
        cell.ThursdayLabel.textColor = [UIColor colorWithHexadecimalString:aRoute.routeTextColor];
    }
    else{
        cell.ThursdayLabel.textColor = [UIColor colorWithHexadecimalString:aRoute.routeColor];
    }
    if ([[trip.calendarDays objectAtIndex:4]integerValue] == 1)
    {
        cell.FridayLabel.textColor = [UIColor colorWithHexadecimalString:aRoute.routeTextColor];
    }
    else{
        cell.FridayLabel.textColor = [UIColor colorWithHexadecimalString:aRoute.routeColor];
    }
    if ([[trip.calendarDays objectAtIndex:5]integerValue] == 1)
    {
        cell.SaturdayLabel.textColor = [UIColor colorWithHexadecimalString:aRoute.routeTextColor];
    }
    else{
        cell.SaturdayLabel.textColor = [UIColor colorWithHexadecimalString:aRoute.routeColor];
    }
    if ([[trip.calendarDays objectAtIndex:6]integerValue] == 1)
    {
        cell.SundayLabel.textColor = [UIColor colorWithHexadecimalString:aRoute.routeTextColor];
    }
    else{
        cell.SundayLabel.textColor = [UIColor colorWithHexadecimalString:aRoute.routeColor];
    }
    
    
    
    return cell;
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end