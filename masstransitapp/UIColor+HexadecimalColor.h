//
//  UIColor+HexadecimalColor.h
//  masstransitapp
//
//  Created by Noemi Quezada on 11/19/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

/* Inspiration from http://stackoverflow.com/questions/6207329/how-to-set-hex-color-code-for-background*/

#import <UIKit/UIKit.h>

@interface UIColor (HexadecimalColor)

+(UIColor *)colorWithHexadecimalString:(NSString *)aHexString;


@end
