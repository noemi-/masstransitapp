//
//  StopsStopCodeBadge.m
//  masstransitapp
//
//  Created by Noemi Quezada on 11/26/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "StopsStopCodeBadge.h"
#import "UIColor+HexadecimalColor.h"
#import "TextView.h"

@implementation StopsStopCodeBadge

@synthesize text;
@synthesize textColor;
@synthesize hexColor;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    /* Set background transparent */
    self.opaque = NO;
    self.backgroundColor = [UIColor clearColor];
    
    // Drawing code
    
    /* Color for Circle */
    UIColor *circleColor = [UIColor colorWithHexadecimalString:hexColor];
    
    /* Add Image to View */
    UIImage *image = [UIImage imageNamed:@"hexagon.png"];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, rect.size.height)];
    [iv setTintColor:circleColor];
    [iv setImage:image];
    [self addSubview:iv];
    [self sendSubviewToBack:iv];
    
    /* Create another subview */
    TextView * tView = [[TextView alloc]initWithFrame:CGRectMake(0, 0, rect.size.width, rect.size.height)];
    
    tView.text = text;
    tView.textColor = textColor;
    tView.opaque = NO;
    tView.backgroundColor = [UIColor clearColor];
    
    [self addSubview:tView];
    [self bringSubviewToFront:tView];
    


    

}


@end
