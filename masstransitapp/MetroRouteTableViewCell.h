//
//  MetroRouteTableViewCell.h
//  masstransitapp
//
//  Created by Noemi Quezada on 11/28/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RouteShortNameBadge.h"

@interface MetroRouteTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *routeDescriptionLabel;
@property (weak, nonatomic) IBOutlet RouteShortNameBadge *routeShortNameBadgeImage;

@end
