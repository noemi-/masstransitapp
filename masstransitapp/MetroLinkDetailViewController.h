//
//  MetroLinkDetailViewController.h
//  masstransitapp
//
//  Created by Noemi Quezada on 12/1/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Stop.h"
#import "Route.h"
#import "StopTime.h"
#import "StopsStopCodeBadge.h"
#import "RouteShortNameBadge.h"
#import "MetroDatabase.h"
#import "StopTimesTableViewCell.h"
#import "UIColor+HexadecimalColor.h" 

@interface MetroLinkDetailViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property(strong, nonatomic)NSArray * times;
@property(strong, nonatomic)Stop * aStop;
@property(strong, nonatomic)Route * aRoute;
@property (weak, nonatomic) IBOutlet UITableView *StopTimesTable;
@property (weak, nonatomic) IBOutlet UILabel *RouteNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *StopNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *routeNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *routeDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *stopCodeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *RouteUIView;
@property (weak, nonatomic) IBOutlet UIImageView *StopUIView;
@property (weak, nonatomic) IBOutlet UIImageView *StopTimesUIView;
@property (weak, nonatomic) IBOutlet UILabel *routeLabel;
@property (weak, nonatomic) IBOutlet UILabel *stopLabel;
@property (weak, nonatomic) IBOutlet UILabel *stopTimesLabel;

@end
