//
//  MetroDatabase.h
//  masstransitapp
//
//  Created by Noemi Quezada on 11/27/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "MassTransitSQLite.h"

@interface MetroDatabase : MassTransitSQLite

+ (MetroDatabase*) database;

@end
