//
//  SQLiteDatabase.m
//  masstransitapp
//
//  Created by Noemi Quezada on 11/15/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "SQLiteDatabase.h"

@implementation SQLiteDatabase

@synthesize databasePath;
@synthesize fileName;

-(SQLiteDatabase *)initWithFile:(NSString *)aFileName
{
    self = [super init];
    if (self) {
        fileName = aFileName;
        databasePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"sl3"];
        if (sqlite3_open([databasePath UTF8String], &_databaseConnection) != SQLITE_OK) {
            NSLog(@"Failed to open database.");
        }
    }
    return self;
}

-(id)init{
    self = [super init];
    if (self)
    {
        databasePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"sl3"];
        if (sqlite3_open([databasePath UTF8String], &_databaseConnection) != SQLITE_OK) {
            NSLog(@"Failed to open database.");
    }
    }
    return self;
    
}

- (void) dealloc
{
    sqlite3_close(_databaseConnection);
}


@end
