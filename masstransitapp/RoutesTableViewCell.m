//
//  RoutesTableViewCell.m
//  masstransitapp
//
//  Created by Noemi Quezada on 11/19/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "RoutesTableViewCell.h"

@implementation RoutesTableViewCell

@synthesize routeLongNameLabel;
@synthesize routeShortNameBadgeImage;




- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        routeLongNameLabel.text = @"Route Long Name";
        ((RouteShortNameBadge *)routeShortNameBadgeImage).text = @"#";
        ((RouteShortNameBadge *)routeShortNameBadgeImage).hexColor = @"FF66FF";
        
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
