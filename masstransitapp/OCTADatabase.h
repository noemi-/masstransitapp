//
//  OCTADatabase.h
//  masstransitapp
//
//  Created by Noemi Quezada on 11/17/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "MassTransitSQLite.h"

@interface OCTADatabase : MassTransitSQLite

+ (OCTADatabase*) database;

@end
