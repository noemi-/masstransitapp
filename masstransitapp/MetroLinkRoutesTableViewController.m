//
//  MetroLinkRoutesTableViewController.m
//  masstransitapp
//
//  Created by Noemi Quezada on 12/1/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "MetroLinkRoutesTableViewController.h"
#import "MetroLinkStopsTableViewController.h"
#import "MetroDatabase.h"
#import "MetroRouteTableViewCell.h"
#import "RoutesTableViewCell.h"
#import "Route.h"

@interface MetroLinkRoutesTableViewController ()

@end

@implementation MetroLinkRoutesTableViewController



@synthesize routes;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    

        MetroDatabase * aDatabase = [MetroDatabase database];
        routes = [[NSArray alloc]initWithArray:[aDatabase allRoutes]];
    
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return [routes count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
        MetroRouteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MetroRoutesCell"];
        
        Route * route = (routes)[indexPath.row];
        
        cell.routeShortNameBadgeImage.text = @"";
        cell.routeShortNameBadgeImage.hexColor = route.routeColor;
        cell.routeShortNameBadgeImage.textColor = route.routeTextColor;
        cell.routeDescriptionLabel.text = route.routeLongName;
        [cell.routeShortNameBadgeImage setNeedsDisplay];
        
        
        return cell;
    
    
    
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
    if ([segue.identifier isEqualToString:@"stopsSegue"])
    {
        MetroLinkStopsTableViewController *stopsVC = segue.destinationViewController;
        NSIndexPath * selectedRowIndex = [self.tableView indexPathForSelectedRow];
        /* Send the Route Record at that index */
        stopsVC.aRoute = [self.routes objectAtIndex:selectedRowIndex.row];
        
    }
}


@end