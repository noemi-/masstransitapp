//
//  Route.m
//  masstransitapp
//
//  Created by Noemi Quezada on 11/17/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "Route.h"

@implementation Route

@synthesize routeId;
@synthesize routeShortName;
@synthesize routeLongName;
@synthesize routeColor;
@synthesize routeTextColor;
@synthesize routeDescription;

-(Route *)initWithRouteId:(NSString *)aRouteId andRouteShortName:(NSString *)aRouteShortName aRouteLongName:(NSString *)aRouteLongName aRouteDescription:(NSString *)aRouteDescription aRouteTextColor:(NSString *)aRouteTextColor andRouteColor:(NSString *)aRouteColor
{
    self = [super init];
    if (self)
    {
        routeId = aRouteId;
        routeShortName =  aRouteShortName;
        routeLongName = aRouteLongName;
        routeDescription = aRouteDescription; 
        routeColor = aRouteColor;
        routeTextColor = aRouteTextColor;
        
    }
    return self; 
}

@end
