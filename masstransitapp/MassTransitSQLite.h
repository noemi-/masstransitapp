//
//  MassTransitSQLite.h
//  masstransitapp
//
//  Created by Noemi Quezada on 11/15/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "SQLiteDatabase.h"
#import "Route.h"
#import "Trip.h"
#import "Stop.h"
#import "StopTime.h"


@interface MassTransitSQLite : SQLiteDatabase
@property(readonly, nonatomic)NSString * agencyName;
@property(readonly, nonatomic)NSString * agencyURL;
@property(readonly, nonatomic)NSString * agencyPhone;

-(NSArray *)allRoutes;
-(NSArray *)tripsWithRouteId:(NSString *)aRouteId;
-(NSArray *)stopsWithRouteId:(NSString *)aRouteId;
-(NSArray *)calendarWithServiceId:(NSString *)aServiceId;
-(NSArray *)stopTimesForStop:(Stop *)aStop;


@end
