//
//  OCTARoutesTableViewController.h
//  masstransitapp
//
//  Created by Noemi Quezada on 11/19/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OCTARoutesTableViewController : UITableViewController

@property(nonatomic, strong)NSArray * routes;

@end
