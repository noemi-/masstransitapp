//
//  StopsTableViewCell.h
//  masstransitapp
//
//  Created by Noemi Quezada on 11/26/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StopsStopCodeBadge.h"

@interface StopsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *StopsNameLabel;
@property (weak, nonatomic) IBOutlet StopsStopCodeBadge *StopCodeBadge;
@property (weak, nonatomic) IBOutlet UIImageView *stopHexagonImage;

@end
