//
//  SQLiteDatabase.h
//  masstransitapp
//
//  Created by Noemi Quezada on 11/15/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface SQLiteDatabase : NSObject
{
    sqlite3 * _databaseConnection;

}

@property (retain, nonatomic)NSString * databasePath;
@property (retain, nonatomic)NSString * fileName; 
-(SQLiteDatabase *)initWithFile:(NSString *)aPathName;


@end
