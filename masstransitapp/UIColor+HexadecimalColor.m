//
//  UIColor+HexadecimalColor.m
//  masstransitapp
//
//  Created by Noemi Quezada on 11/19/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "UIColor+HexadecimalColor.h"

@implementation UIColor (HexadecimalColor)


+(UIColor *)colorWithHexadecimalString:(NSString *)aHexString
{
    /* Seperate String by red green and blue */
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [aHexString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [aHexString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [aHexString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

@end
