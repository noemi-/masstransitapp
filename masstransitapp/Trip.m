//
//  Trip.m
//  masstransitapp
//
//  Created by Noemi Quezada on 11/22/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "Trip.h"

@implementation Trip

@synthesize routeId;
@synthesize tripId;
@synthesize calendarDays;

-(Trip *)initWithRouteId:(NSString *)aRouteId withCalendarDays:(NSArray *)someCalendarDays andWithTripId:(NSInteger)aTripId
{
    self = [super init];
    if (self)
    {
        routeId = aRouteId;
        tripId = aTripId;
        calendarDays = [[NSArray alloc]initWithArray:someCalendarDays]; 
    }
    return self; 
}

@end
