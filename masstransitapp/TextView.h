//
//  TextView.h
//  masstransitapp
//
//  Created by Noemi Quezada on 12/1/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextView : UIView

@property(strong, nonatomic)NSString * textColor;
@property(strong, nonatomic)NSString * text;

@end
