//
//  MassTransitSQLite.m
//  masstransitapp
//
//  Created by Noemi Quezada on 11/15/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "MassTransitSQLite.h"


@implementation MassTransitSQLite

@synthesize agencyName;
@synthesize agencyPhone;
@synthesize agencyURL;


-(NSString *)stopNameWithStopId:(NSString*)aStopId
{
    NSString * query = [NSString stringWithFormat:@"SELECT stop_name FROM stops WHERE stop_id = '%@'", aStopId];
    sqlite3_stmt * statement;
    const unsigned char * text;
    NSString *stopName;
    
    
    if (sqlite3_prepare_v2(_databaseConnection, [query UTF8String], (int)[query length], &statement, nil)==SQLITE_OK)
    {
        while (sqlite3_step(statement)== SQLITE_ROW)
        {
            
            
            /* Stop Name */
            text = sqlite3_column_text(statement, 0);
            if (text)
            {
                stopName= [NSString stringWithCString:(const char *)text encoding:NSUTF8StringEncoding];
            }
            else{
                stopName = nil;
            }
        }
        sqlite3_finalize(statement);
    }
    else{
        NSLog(@"Routes Query Error");
    }
    
    return stopName;
    
}

-(NSString *)formattedTime:(NSString *)aTime
{
    NSString * formattedTime;
    if ([aTime length] != 0)
    {
    NSString *theHour = [aTime substringToIndex:2];
    NSString *theMinutes = [aTime substringFromIndex:2];
    theMinutes = [theMinutes substringToIndex:3];


    if ([theHour integerValue] > 12)
    {
        theHour = [NSString stringWithFormat:@"%ld",([theHour integerValue]-12)];
        theMinutes = [NSString stringWithFormat:@"%@ pm", theMinutes];
    }
    else{
        theHour = [NSString stringWithFormat:@"%ld", (long)[theHour integerValue]];
        if ([theHour integerValue] == 12)
        {
            theMinutes = [NSString stringWithFormat:@"%@ pm", theMinutes];
        }
        else{
            theMinutes = [NSString stringWithFormat:@"%@ am", theMinutes];
        }

    }

    formattedTime = [theHour stringByAppendingString:theMinutes];
    }

    return formattedTime;
}

-(NSString *)stopCodeForStopId:(NSString *)stopId
{
    NSString *stopCode;
    NSString * query = [NSString stringWithFormat:@"SELECT stop_code FROM stops WHERE stop_id = '%@'", stopId];
    sqlite3_stmt * statement;
    const unsigned char * text;

    if (sqlite3_prepare_v2(_databaseConnection, [query UTF8String], (int)[query length], &statement, nil)==SQLITE_OK)
    {
        while (sqlite3_step(statement)== SQLITE_ROW)
        {
            
            /* Stop Code */
            text = sqlite3_column_text(statement, 0);
            if (text)
            {
                stopCode= [NSString stringWithCString:(const char *)text encoding:NSUTF8StringEncoding];
                
            }
            else{
                stopCode = nil;
            }
        }
        sqlite3_finalize(statement);
    }
    else{
        NSLog(@"Stop Code Query Error");
        stopCode = @"";
    }
    
    return stopCode;
    
}

-(NSArray *)stopTimesForStop:(Stop *)aStop
{
    
    NSArray * allTrips = [aStop tripIds];
    NSMutableArray * rv = [[NSMutableArray alloc]init];
    for (id object in allTrips)
    {
        /* For each trip */
        Trip * aTrip = object;
        NSString * query = [NSString stringWithFormat:@"SELECT * FROM stop_times WHERE trip_id = %ld", (long)aTrip.tripId];
        sqlite3_stmt * statement;
        const unsigned char * text;
        NSString *arrivalTime, *departureTime;


        if (sqlite3_prepare_v2(_databaseConnection, [query UTF8String], (int)[query length], &statement, nil)==SQLITE_OK)
        {
            while (sqlite3_step(statement)== SQLITE_ROW)
            {
                
                /* Arrival Time */
                text = sqlite3_column_text(statement, 1);
                if (text)
                {
                    arrivalTime= [NSString stringWithCString:(const char *)text encoding:NSUTF8StringEncoding];
                    arrivalTime = [self formattedTime:arrivalTime];

                }
                else{
                    arrivalTime = nil;
                }
                
                /* Departure Time */
                text = sqlite3_column_text(statement, 2);
                if (text)
                {
                    departureTime= [NSString stringWithCString:(const char *)text encoding:NSUTF8StringEncoding];
                    departureTime = [self formattedTime:departureTime];

                }
                else{
                    departureTime = nil;
                }
                
            }
            sqlite3_finalize(statement);
        }
        
        else{
            NSLog(@"Stop Times Query Error");
        }
        
        StopTime * thisStopTime = [[StopTime alloc]initWithArrivalTime:arrivalTime withDepartureTime:departureTime andWithATrip:aTrip];
        
        /* Add object to array */
        [rv addObject:thisStopTime];
    }
    
    return rv;
    
    
}

-(NSArray *)stopsWithRouteId:(NSString *)aRouteId
{
    NSArray * allTrips = [self tripsWithRouteId:aRouteId];
    Trip * aTrip = [allTrips objectAtIndex:0];
    
    NSMutableArray * rv = [[NSMutableArray alloc]init];
    NSString * query = [NSString stringWithFormat:@"SELECT * FROM stop_times WHERE trip_id = %ld ORDER BY stop_sequence ASC", (long)aTrip.tripId];
    sqlite3_stmt * statement;
    const unsigned char * text;
    NSString *routeId, *stopId, *stopName, *stopCode;
    NSInteger stopSequence, tripId;
    
    if (sqlite3_prepare_v2(_databaseConnection, [query UTF8String], (int)[query length], &statement, nil)==SQLITE_OK)
    {
        while (sqlite3_step(statement)== SQLITE_ROW)
        {
            /* Trip Id */
            tripId = sqlite3_column_int(statement, 0);
            
            
            /* Stop Id */
            text = sqlite3_column_text(statement, 3);
            if (text)
            {
                stopId= [NSString stringWithCString:(const char *)text encoding:NSUTF8StringEncoding];
            }
            else{
                stopId = nil;
            }
            
            /* Stop Sequence */
            stopSequence = sqlite3_column_int(statement, 4);
            
            /* Stop Name */
            stopName = [self stopNameWithStopId:stopId];
            stopName = [stopName capitalizedString];
            
            stopCode = [self stopCodeForStopId:stopId];
            
            routeId = aRouteId;
            
            Stop *thisStop = [[Stop alloc]initWithRouteId:routeId withArrayOfTripIds:allTrips withStopId:stopId withStopCode:stopCode withStopName:stopName withStopSequence:stopSequence];
            
            /* Add object to array */
            [rv addObject:thisStop];
        }
        
        sqlite3_finalize(statement);
    }
    
    else{
        NSLog(@"Stops Query Error");
    }
    
    return rv;
    
    
    
}



-(NSArray *)tripsWithRouteId:(NSString *)aRouteId
{
    NSMutableArray * rv = [[NSMutableArray alloc]init];
    NSString * query = [NSString stringWithFormat:@"SELECT * FROM trips WHERE route_id = '%@'", aRouteId];
    sqlite3_stmt * statement;
    const unsigned char * text;
    NSString *routeId, *serviceId;
    NSArray  * calendarDays = [[NSArray alloc]init];
    NSInteger tripId;
    
    if (sqlite3_prepare_v2(_databaseConnection, [query UTF8String], (int)[query length], &statement, nil)==SQLITE_OK)
    {
        while (sqlite3_step(statement)== SQLITE_ROW)
        {
            /* Route Id */
            text = sqlite3_column_text(statement, 0);
            if (text)
            {
                routeId = [NSString stringWithCString:(const char *)text encoding:NSUTF8StringEncoding];
            }
            else{
                routeId = aRouteId;
            }
            
            /* Service Id for Calendar */
            text = sqlite3_column_text(statement, 1);
            if (text)
            {
                serviceId = [NSString stringWithCString:(const char *)text encoding:NSUTF8StringEncoding];
            }
            else{
                serviceId = nil;
            }
            
            /* Get Calendar Days */
            calendarDays = [self calendarWithServiceId:serviceId];
            
            tripId =sqlite3_column_int(statement, 2);
            
            Trip *thisTrip = [[Trip alloc]initWithRouteId:routeId withCalendarDays:calendarDays andWithTripId:tripId];
            
            /* Add object to array */
            [rv addObject:thisTrip];
        }
        
        sqlite3_finalize(statement);
    }
    else{
        NSLog(@"Trips Query Error");
    }
    
    return rv;
}

-(NSArray *)calendarWithServiceId:(NSString *)aServiceId
{
    NSString * query = [NSString stringWithFormat:@"SELECT * FROM calendar WHERE service_id = '%@'", aServiceId];
    sqlite3_stmt * statement;
    NSMutableArray * calendarDays = [[NSMutableArray alloc]init];
    
    if (sqlite3_prepare_v2(_databaseConnection, [query UTF8String], (int)[query length], &statement, nil)==SQLITE_OK)
    {
        while (sqlite3_step(statement)== SQLITE_ROW)
        {
            
            
            for (int i = 1; i < 8; i++)
            {
                NSNumber * aNumber = [[NSNumber alloc]initWithInt:sqlite3_column_int(statement, i)];
                [calendarDays addObject:aNumber];
            }
        }
        
        sqlite3_finalize(statement);
    }
    else{
        NSLog(@"Calendar Query Error");
    }
    return calendarDays;
}

-(NSArray *)allRoutes
{
    NSMutableArray * rv = [[NSMutableArray alloc]init];
    NSString * query = @"SELECT * FROM routes";
    sqlite3_stmt *statement;
    const unsigned char *text;
    NSString * routeId, *routeShortName, *routeLongName, *routeColor, *routeDescription, *routeTextColor;
    
    if (sqlite3_prepare_v2(_databaseConnection, [query UTF8String], (int)[query length], &statement, nil)== SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            /* Route ID */
            text = sqlite3_column_text(statement, 0);
            if (text)
            {
                routeId = [NSString stringWithCString:(const char *)text encoding:NSUTF8StringEncoding];
            }
            else{
                routeId = nil;
            }
            
            /* Route Description */
            text = sqlite3_column_text(statement, 4);
            if (text)
            {
                routeDescription = [NSString stringWithCString:(const char *)text encoding:NSUTF8StringEncoding];
            }
            else{
                routeDescription = nil;
            }
            
            
            /* Route Short Name */
            text = sqlite3_column_text(statement, 2);
            if (text)
            {
                routeShortName = [NSString stringWithCString:(const char *)text encoding:NSUTF8StringEncoding];
            }
            else{
                routeShortName = nil;
            }
            
            /* Route Long Name */
            text = sqlite3_column_text(statement, 3);
            if (text)
            {
                routeLongName = [NSString stringWithCString:(const char *)text encoding:NSUTF8StringEncoding];
            }
            else{
                routeLongName = nil;
            }
            
            /* Route Color */
            text = sqlite3_column_text(statement, 7);
            if (text)
            {
                routeColor = [NSString stringWithCString:(const char *)text encoding:NSUTF8StringEncoding];
                if ([routeColor length] == 0)
                {
                    routeColor = @"0B974C";
                }
            }
            else{
                routeColor = nil;
            }
            
            /* Route Text Color */
            text = sqlite3_column_text(statement, 8);
            if (text)
            {
                
                routeTextColor = [NSString stringWithCString:(const char *)text encoding:NSUTF8StringEncoding];
                if ([routeTextColor length] == 0)
                {
                    routeTextColor = @"ffffff";
                }
            }
            else{
                routeTextColor = nil;
            }
            
            Route *thisRoute = [[Route alloc]initWithRouteId:routeId andRouteShortName:routeShortName aRouteLongName:routeLongName aRouteDescription:routeDescription aRouteTextColor:routeTextColor andRouteColor:routeColor]; 
            
            /* Add object to array */
            [rv addObject:thisRoute];
        }
        
        sqlite3_finalize(statement);
    }
    else{
        NSLog(@"Routes Query Error");
    }
    
    
    
    return rv;
    
}

@end
