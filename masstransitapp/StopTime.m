//
//  StopTime.m
//  masstransitapp
//
//  Created by Noemi Quezada on 11/22/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "StopTime.h"

@implementation StopTime

@synthesize arrivalTime;
@synthesize departureTime;
@synthesize trip;

-(StopTime *)initWithArrivalTime:(NSString *)anArrivalTime withDepartureTime:(NSString *)aDeparturedTime andWithATrip:(Trip *)aTrip
{
    self = [super init];
    if (self)
    {
        arrivalTime = anArrivalTime;
        departureTime = aDeparturedTime;
        trip = aTrip; 
    }
    return self; 
}

@end
