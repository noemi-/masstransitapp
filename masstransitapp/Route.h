//
//  Route.h
//  masstransitapp
//
//  Created by Noemi Quezada on 11/17/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Route : NSObject
@property (strong, nonatomic)NSString * routeId;
@property (strong, nonatomic)NSString * routeShortName;
@property (strong, nonatomic)NSString * routeLongName;
@property (strong, nonatomic)NSString * routeDescription;
@property (strong, nonatomic)NSString * routeColor;
@property (strong, nonatomic)NSString * routeTextColor;


-(Route *)initWithRouteId: (NSString *)aRouteId andRouteShortName:(NSString *)aRouteShortName aRouteLongName:(NSString *)aRouteLongName aRouteDescription:(NSString *)aRouteDescription aRouteTextColor: (NSString *)aRouteTextColor andRouteColor: (NSString *)aRouteColor;
@end
