//
//  StopTimesTableViewCell.h
//  masstransitapp
//
//  Created by Noemi Quezada on 11/27/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StopTimesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *ArrivalTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *DepartureTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *MondayLabel;
@property (weak, nonatomic) IBOutlet UILabel *TuesdayLabel;
@property (weak, nonatomic) IBOutlet UILabel *WednesdayLabel;
@property (weak, nonatomic) IBOutlet UILabel *ThursdayLabel;
@property (weak, nonatomic) IBOutlet UILabel *FridayLabel;
@property (weak, nonatomic) IBOutlet UILabel *SaturdayLabel;
@property (weak, nonatomic) IBOutlet UILabel *SundayLabel;
@property (weak, nonatomic) IBOutlet UIImageView *calendarView;


@end
