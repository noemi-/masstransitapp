//
//  StopsStopCodeBadge.h
//  masstransitapp
//
//  Created by Noemi Quezada on 11/26/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StopsStopCodeBadge : UIView

@property(strong, nonatomic)NSString * hexColor;
@property(strong, nonatomic)NSString * textColor; 
@property(strong, nonatomic)NSString * text;

@end
