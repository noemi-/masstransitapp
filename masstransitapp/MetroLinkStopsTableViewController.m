//
//  MetroLinkStopsTableViewController.m
//  masstransitapp
//
//  Created by Noemi Quezada on 12/1/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import "MetroLinkStopsTableViewController.h"
#import "MetroLinkDetailViewController.h"
#import "StopsTableViewCell.h"

@interface MetroLinkStopsTableViewController ()

@end

@implementation MetroLinkStopsTableViewController


@synthesize stops;
@synthesize aRoute;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    

        if ([aRoute.routeId isEqualToString:@"Burbank-Bob Hope Airport"])
        {
            stops = [[NSArray alloc]init];
            
            UIAlertView *alert = [[UIAlertView alloc]
                                  
                                  initWithTitle:@"Error!"
                                  message:[NSString stringWithFormat:@"No Stops for %@", aRoute.routeId]
                                  delegate:nil
                                  cancelButtonTitle:@"Dismiss"
                                  otherButtonTitles:nil];
            
            [alert show];
            
        }
        else{
            MetroDatabase * aDatabase = [MetroDatabase database];
            stops = [[NSArray alloc]initWithArray:[aDatabase stopsWithRouteId:aRoute.routeId]];
            NSLog(@"%ld", [stops count]);
        }
    
    
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return [stops count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    StopsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"StopCell" forIndexPath:indexPath];
    
    Stop * stop = (stops)[indexPath.row];
    cell.StopsNameLabel.text = stop.stopName;
    cell.StopCodeBadge.text = stop.stopCode;
    cell.StopCodeBadge.hexColor = aRoute.routeColor;
    cell.StopCodeBadge.textColor = aRoute.routeTextColor;
    
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSLog(@"prepareForSegue: %@", segue.identifier);
    if ([segue.identifier isEqualToString:@"timesSegue"])
    {
        MetroLinkDetailViewController *timesVC = segue.destinationViewController;
        NSIndexPath * selectedRowIndex = [self.tableView indexPathForSelectedRow];
        /* Send the Stop Record at that index */
        timesVC.aStop = [self.stops objectAtIndex:selectedRowIndex.row];
        timesVC.aRoute = self.aRoute;
    }
}


@end
