//
//  MetroLinkRoutesTableViewController.h
//  masstransitapp
//
//  Created by Noemi Quezada on 12/1/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MetroLinkRoutesTableViewController : UITableViewController

@property(nonatomic, strong)NSArray * routes;
@property(nonatomic, assign)NSInteger tab;

@end
